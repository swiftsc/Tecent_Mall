package com.sc.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by sc on 2017/10/21.
 */

@Data
public class CartVo {

    private List<CartProductVo> cartProductVoList;
    private BigDecimal cartTotalPrice;
    private Boolean allChecked;
    private String imageHost;
}
