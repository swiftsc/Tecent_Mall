package com.sc.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by sc on 2017/10/28.
 */
@Data
public class OrderProductVo {
    private List<OrderItemVo> orderItemVoList;
    private BigDecimal productTotalPrice;
    private String imageHost;
}
