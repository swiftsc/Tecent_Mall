package com.sc.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by sc on 2017/10/21.
 */
@Data
public class CartProductVo {
    private Integer id;
    private Integer userId;
    private Integer productId;
    private Integer quantity;


    private Integer productChecked;
    private String productName;
    private String productSubtitle;
    private String productMainImage;
    private BigDecimal productPrice;
    private Integer productStatus;
    private BigDecimal productTotalPrice;
    private Integer productStock;
    private String limitQuantity;

}
