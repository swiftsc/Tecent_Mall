package com.sc.vo;

import lombok.Data;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * Created by sc on 2017/10/16.
 */

@Data
public class ProductListVo {
    private Integer id;
    private Integer categoryId;
    private String name;
    private Integer status;
    private String subtitle;
    private BigDecimal price;
    private String mainImage;
    private String imageHost;
}
