package com.sc.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {
    private Integer id;

    @NotNull
    private Integer categoryId;

    @NotNull
    private String name;

    private String subtitle;

    private String mainImage;

    private String subImages;

    private String detail;

    @NotNull
    private BigDecimal price;

    @NotNull
    private Integer stock;


    private Integer status;

    private Date createTime;

    private Date updateTime;



}