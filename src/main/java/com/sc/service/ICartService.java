package com.sc.service;

import com.sc.common.ServerResponse;

/**
 * Created by sc on 2017/10/21.
 */
public interface ICartService {
    ServerResponse add(Integer userId, Integer count, Integer productId);

    ServerResponse update(Integer userId, Integer count, Integer productId);

    ServerResponse delete(Integer userId, String productIds);

    ServerResponse list(Integer id);

    ServerResponse selectorUnselect(Integer userId, Integer checked, Integer productId);

    ServerResponse getCartProductCount(Integer id);
}
