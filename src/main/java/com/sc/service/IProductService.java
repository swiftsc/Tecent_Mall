package com.sc.service;

import com.sc.common.ServerResponse;
import com.sc.pojo.Product;

/**
 * Created by sc on 2017/10/15.
 */
public interface IProductService {
    ServerResponse saveOrUpdate(Product product);

    ServerResponse setSaleStatus(Integer productId, Integer status);

    ServerResponse manageProductDetail(Integer productId);

    ServerResponse getList(int pageNum, int pageSize);

    ServerResponse getDetail(Integer productId);

    ServerResponse getProductByKeywordAndCategory(int pageNum, int pageSize, String keyword, Integer categoryId, String orderBy);

    ServerResponse searchProduct(String productName, Integer productId, Integer pageNum, Integer pageSize);
}
