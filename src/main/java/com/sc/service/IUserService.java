package com.sc.service;

import com.sc.common.ServerResponse;
import com.sc.dto.UserUpdateDto;
import com.sc.pojo.User;
import org.springframework.stereotype.Service;


public interface IUserService {
    ServerResponse<User> login(String username, String password);

    ServerResponse<String> register(User user);

    ServerResponse<String> selectQuestion(String username);

    ServerResponse<String> checkAnswer(String username, String question, String answer);

    ServerResponse<String> forgetResetPassword(String username, String passwordnew, String token);

    ServerResponse<User> updateInformation(UserUpdateDto user);

    ServerResponse<String> resetPassword(User user, String passwordold, String passwordnew);

    ServerResponse<User> getinformation(Integer userId);

    ServerResponse checkAdmin(User user);
}
