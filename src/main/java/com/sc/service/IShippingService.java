package com.sc.service;

import com.sc.common.ServerResponse;
import com.sc.pojo.Shipping;

/**
 * Created by sc on 2017/10/22.
 */
public interface IShippingService {
    ServerResponse add(Integer userId, Shipping shipping);

    ServerResponse delete(Integer id, Integer shippingId);

    ServerResponse update(Integer id, Shipping shipping);

    ServerResponse select(Integer id, Integer shippingId);

    ServerResponse list(Integer id, Integer pageNum, Integer pageSize);
}
