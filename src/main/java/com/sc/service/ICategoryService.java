package com.sc.service;

import com.sc.common.ServerResponse;
import com.sc.pojo.Category;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sc on 2017/10/15.
 */
public interface ICategoryService {
    ServerResponse add(String categoryName, Integer parentId);

    ServerResponse updateCategory(String categoryName, Integer categoryId);

    ServerResponse<List<Category>> selectParallelCategory(Integer categoryId);

    ServerResponse<List<Integer>> selectDeepCategory(Integer parentId);
}
