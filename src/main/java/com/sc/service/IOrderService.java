package com.sc.service;

import com.sc.common.ServerResponse;

import java.util.Map;

/**
 * Created by sc on 2017/10/22.
 */
public interface IOrderService {
    ServerResponse pay(Integer userId, Long orderNo, String path);

    ServerResponse aliCallBack(Map<String, String> params);

    ServerResponse query(Integer id, Long orderNo);

    ServerResponse createOrder(Integer userId, Integer shippingId);

    ServerResponse cancelOrder(Integer id, Long orderNo);

    ServerResponse getOrderCartProduct(Integer id);

    ServerResponse getDetail(Integer id, Long orderNo);

    ServerResponse getList(Integer id, Integer pageNum, Integer pageSize);

    ServerResponse manageOrderList(Integer pageNum, Integer pageSize);

    ServerResponse manageOrderDetail(Long orderNo);

    ServerResponse search(Long orderNo, Integer pageNum, Integer pageSize);

    ServerResponse sendGoods(Long orderNo);

    void closeOrder(int hour);
}
