package com.sc.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by sc on 2017/10/19.
 */
public interface IFileService {
    String upload(MultipartFile file, String path);


}
