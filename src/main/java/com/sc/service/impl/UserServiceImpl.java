package com.sc.service.impl;

import com.sc.common.*;
import com.sc.dao.UserMapper;
import com.sc.dto.UserUpdateDto;
import com.sc.pojo.User;
import com.sc.service.IUserService;
import com.sc.util.MD5Util;
import com.sc.util.RedisShardedPoolUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;


@Service("userService")
@Slf4j
public class UserServiceImpl implements IUserService {

    private static final String TOKEN_PREFIX = "token_";
    @Autowired
    private UserMapper userMapper;

    /***
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    @Override
    public ServerResponse<User> login(String username, String password) {
        if (userMapper.checkUsername(username) == 0) {
            return ServerResponse.createByErrorMessage(UserStatus.NO_USER.getDesc());
        }
        String ps = MD5Util.MD5EncodeUtf8(password);
        User user = userMapper.selectLogin(username,ps);
        if (user == null) {
            return ServerResponse.createByErrorMessage(UserStatus.PWD_ERROR.getDesc());
        }
        user.setPassword(StringUtils.EMPTY);
        return ServerResponse.createBySuccessDataMessage(UserStatus.LOGIN_SUCCESS.getDesc(), user);
    }

    /***
     * 注册
     *
     * @param user
     * @return
     */
    @Override
    public ServerResponse<String> register(User user) {
        if (userMapper.checkUsername(user.getUsername()) > 0) {
            return ServerResponse.createByErrorMessage(UserStatus.USERNAME_EXIST.getDesc());
        }
        if (userMapper.checkEmail(user.getEmail()) > 0) {
            return ServerResponse.createByErrorMessage(UserStatus.EMAIL_EXIST.getDesc());
        }
        user.setRole(UserStatus.Role.ROLE_CUSTOMER);
        user.setPassword(MD5Util.MD5EncodeUtf8(user.getPassword()));
        int count = userMapper.insert(user);
        if (count == 0) {
            return ServerResponse.createByErrorMessage(UserStatus.REGISTER_FAILED.getDesc());
        }
        return ServerResponse.createBySuccessMessage(UserStatus.REGISTER_SUCCESS.getDesc());

    }

    /***
     * 检查用户名和邮箱重复性
     *
     * @param str
     * @param type
     * @return
     */
    public ServerResponse<String> checkValid(String str, String type) {
        if (StringUtils.isNotBlank(type)) {
            if (Const.USERNAME.equals(type)) {
                int count = userMapper.checkUsername(str);
                if (count > 0) {
                    return ServerResponse.createByErrorMessage(UserStatus.USERNAME_EXIST.getDesc());
                }
            }
            if (Const.EMAIL.equals(type)) {
                int count = userMapper.checkEmail(str);
                if (count > 0) {
                    return ServerResponse.createByErrorMessage(UserStatus.EMAIL_EXIST.getDesc());
                }
            }
        } else {
            return ServerResponse.createByErrorMessage(ResponseCode.ILLEGAL_ARGU.getDesc());
        }

        return ServerResponse.createBySuccessMessage(ResponseCode.SUCCESS.getDesc());
    }

    /***
     * 修改密码或忘记密码获取问题
     *
     * @param username
     * @return
     */
    @Override
    public ServerResponse<String> selectQuestion(String username) {
        if (checkValid(username, UserStatus.USERNAME).isSuccess()) {
            return ServerResponse.createByErrorMessage(UserStatus.NO_USER.getDesc());
        }
        String question = userMapper.selectQuestionByUsername(username);
        if (StringUtils.isNotBlank(question)) {
            return ServerResponse.createBySuccess(question);
        }
        return ServerResponse.createByErrorMessage(UserStatus.QUESTION_EMPTY.getDesc());
    }

    /***
     * 检查问题的答案是否正确
     *
     * @param username
     * @param question
     * @param answer
     * @return
     */
    @Override
    public ServerResponse<String> checkAnswer(String username, String question, String answer) {
        if (userMapper.checkAnswer(username, question, answer) > 0) {
            String forgetToken = UUID.randomUUID().toString();
            RedisShardedPoolUtil.setEx(TOKEN_PREFIX+username,forgetToken,60*60*12);
            return ServerResponse.createBySuccess(forgetToken);
        }
        return ServerResponse.createByErrorMessage(UserStatus.ANSWER_ERROR.getDesc());
    }

    /***
     * 通过问题验证修改密码
     *
     * @param username
     * @param passwordnew
     * @param token
     * @return
     */
    @Override
    public ServerResponse<String> forgetResetPassword(String username, String passwordnew, String token) {
        if (StringUtils.isBlank(token)) {
            return ServerResponse.createByErrorMessage(UserStatus.TOKEN_EMPTY.getDesc());
        }
        if (checkValid(username, UserStatus.USERNAME).isSuccess()) {
            return ServerResponse.createByErrorMessage(UserStatus.NO_USER.getDesc());
        }
        String cacheToken = RedisShardedPoolUtil.get(TOKEN_PREFIX+username);
        if (StringUtils.isBlank(cacheToken)) {
            return ServerResponse.createByErrorMessage(UserStatus.TOKEN_INVALID.getDesc());
        }
        if (StringUtils.equals(token, cacheToken)) {
            String md5pwd = MD5Util.MD5EncodeUtf8(passwordnew);
            if (userMapper.updatePasswordByUsername(username, md5pwd) == 0) {
                return ServerResponse.createByErrorMessage(UserStatus.PASSWORD_RESET_FAILED.getDesc());
            } else {
                return ServerResponse.createBySuccessMessage(UserStatus.PASSWORD_RESET_SUCCESS.getDesc());
            }
        }
        return ServerResponse.createByErrorMessage(UserStatus.TOKEN_INVALID.getDesc());
    }

    /***
     * 用户信息更新
     *
     * @param user
     * @return
     */
    @Override
    public ServerResponse<User> updateInformation(UserUpdateDto user) {
        if (userMapper.checkEmailByUserID(user.getId(), user.getEmail()) > 0) {
            return ServerResponse.createByErrorMessage(UserStatus.EMAIL_EXIST.getDesc());
        }
        User updateUser = new User();
        BeanUtils.copyProperties(user, updateUser);

        if (userMapper.updateByPrimaryKeySelective(updateUser) > 0) {
            return ServerResponse.createBySuccessDataMessage(UserStatus.INFORMATION_MODIFY_SUCCESS.getDesc(), updateUser);
        }
        return ServerResponse.createByErrorMessage(UserStatus.INFORMATION_MODIFY_FAILED.getDesc());

    }

    /***
     * 通过旧密码修改密码
     *
     * @param user
     * @param passwordold
     * @param passwordnew
     * @return
     */
    @Override
    public ServerResponse<String> resetPassword(User user, String passwordold, String passwordnew) {
        //防止横向越权，一定要当前用户名对应当前密码
        if (userMapper.checkPassword(user.getId(), MD5Util.MD5EncodeUtf8(passwordold)) == 0) {
            return ServerResponse.createByErrorMessage(UserStatus.OLDPWD_ERROR.getDesc());
        }
        user.setPassword(MD5Util.MD5EncodeUtf8(passwordnew));
        if (userMapper.updateByPrimaryKeySelective(user) > 0) {
            return ServerResponse.createBySuccessMessage(UserStatus.PASSWORD_RESET_SUCCESS.getDesc());
        }
        return ServerResponse.createByErrorMessage(UserStatus.PASSWORD_RESET_FAILED.getDesc());
    }

    @Override
    public ServerResponse<User> getinformation(Integer userId) {
        User user = userMapper.selectByPrimaryKey(userId);
        if (user == null) {
            return ServerResponse.createByErrorMessage(UserStatus.NO_USER.getDesc());
        }
        user.setPassword(StringUtils.EMPTY);
        return ServerResponse.createBySuccess(user);
    }

    @Override
    public ServerResponse checkAdmin(User user) {
        if (user != null && user.getRole().intValue() == UserStatus.Role.ROLE_ADMIN) {
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createByError();
    }


}
