package com.sc.service.impl;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.sc.common.CategoryStatus;
import com.sc.common.ServerResponse;
import com.sc.dao.CategoryMapper;
import com.sc.pojo.Category;
import com.sc.service.ICategoryService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Set;

/**
 * Created by sc on 2017/10/15.
 */
@Service
@Slf4j
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    private CategoryMapper categoryMapper;


    @Override
    public ServerResponse add(String categoryName, Integer parentId) {
        if (categoryName == null || parentId == null) {
            return ServerResponse.createByErrorMessage(CategoryStatus.ILLEGAL_PARAM.getDesc());
        }
        if (categoryMapper.checkParentIdExist(parentId) == 0) {
            return ServerResponse.createByErrorMessage(CategoryStatus.PARENT_ID_ERROR.getDesc());
        }
        Category category = new Category();
        category.setName(categoryName);
        category.setParentId(parentId);
        category.setStatus(true);
        if (categoryMapper.insert(category) > 0) {
            return ServerResponse.createBySuccessMessage(CategoryStatus.ADD_SUCCESS.getDesc());
        }
        return ServerResponse.createByErrorMessage(CategoryStatus.ADD_FAILED.getDesc());
    }

    @Override
    public ServerResponse updateCategory(String categoryName, Integer categoryId) {
        Category category = categoryMapper.selectByPrimaryKey(categoryId);
        if (category == null) {
            return ServerResponse.createByErrorMessage(CategoryStatus.NO_CATEGORY.getDesc());
        }
        category.setName(categoryName);
        category.setId(categoryId);
        if (categoryMapper.updateByPrimaryKeySelective(category) > 0) {
            return ServerResponse.createBySuccessMessage(CategoryStatus.MODIFY_SUCCESS.getDesc());
        }
        return ServerResponse.createByErrorMessage(CategoryStatus.MODIFY_FAILED.getDesc());
    }

    @Override
    public ServerResponse<List<Category>> selectParallelCategory(Integer categoryId) {
        List<Category> categoryList = categoryMapper.selectChildCategoryByParentID(categoryId);
        if (CollectionUtils.isEmpty(categoryList)) {
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createBySuccess(categoryList);
    }

    @Override
    public ServerResponse<List<Integer>> selectDeepCategory(Integer parentId) {
        Set<Category> categorySet = Sets.newHashSet();
        findChildCategory(parentId, categorySet);
        List<Integer> IdList = Lists.newArrayList();
        for (Category item : categorySet) {
            IdList.add(item.getId());
        }
        return ServerResponse.createBySuccess(IdList);
    }


    /***
     * 递归查询
     *
     * @param category
     * @return
     */
    private Set<Category> findChildCategory(Integer categoryId, Set<Category> categorySet) {
        Category category = categoryMapper.selectByPrimaryKey(categoryId);
        if (category != null) {
            categorySet.add(category);
        }
        //子节点
        List<Category> categoryList = categoryMapper.selectChildCategoryByParentID(categoryId);
        for (Category item : categoryList) {
            findChildCategory(item.getId(), categorySet);
        }
        return categorySet;
    }
}
