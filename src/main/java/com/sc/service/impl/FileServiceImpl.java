package com.sc.service.impl;

import com.google.common.collect.Lists;
import com.sc.service.IFileService;
import com.sc.util.FTPUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by sc on 2017/10/19.
 */
@Service
@Slf4j
public class FileServiceImpl implements IFileService {



    @Override
    public String upload(MultipartFile file, String path) {
        String filename = file.getOriginalFilename();
        String extname = filename.substring(filename.lastIndexOf(".") + 1);
        String uploadName = UUID.randomUUID().toString() + "." + extname;
        log.info("上传文件名{},原始文件名{},上传路径{}", uploadName, filename, path);
        File dir = new File(path);
        if (!dir.exists()) {
            dir.setWritable(true);
            dir.mkdirs();
        }
        File targetFile = new File(path, uploadName);
        try {
            file.transferTo(targetFile);
            FTPUtil.uploadFile(Lists.newArrayList(targetFile));
            System.out.println("上传成功");
            targetFile.delete();
        } catch (IOException e) {
            log.error("上传图片出错了");
            return null;
        }
        return targetFile.getName();

    }
}

