package com.sc.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.sc.common.ServerResponse;
import com.sc.common.ShippingStatus;
import com.sc.dao.ShippingMapper;
import com.sc.pojo.Shipping;
import com.sc.service.IShippingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by sc on 2017/10/22.
 */
@Service
@Slf4j
public class ShippingServiceImpl implements IShippingService {

    @Autowired
    private ShippingMapper shippingMapper;

    @Override
    public ServerResponse add(Integer userId, Shipping shipping) {
        shipping.setUserId(userId);
        if (shippingMapper.insert(shipping) > 0) {
            Map result = Maps.newHashMap();
            result.put("shippingId", shipping.getId());
            return ServerResponse.createBySuccessDataMessage("新建地址成功", result);
        }
        return ServerResponse.createByErrorMessage(ShippingStatus.ADD_ADDRESS_FAIL.getDesc());
    }

    @Override
    public ServerResponse delete(Integer id, Integer shippingId) {
        if (shippingMapper.deleteByUserIdAndShippingId(id, shippingId) > 0) {
            return ServerResponse.createBySuccessMessage(ShippingStatus.DEL_SUCCESS.getDesc());
        }
        return ServerResponse.createByErrorMessage(ShippingStatus.DEL_FAIL.getDesc());
    }

    @Override
    public ServerResponse update(Integer id, Shipping shipping) {
        shipping.setUserId(id);
        if (shippingMapper.updateByUserId(shipping) > 0) {
            return ServerResponse.createBySuccessMessage(ShippingStatus.UPDATE_SUCCESS.getDesc());
        }
        return ServerResponse.createByErrorMessage(ShippingStatus.UPDATE_FAIL.getDesc());
    }

    @Override
    public ServerResponse select(Integer id, Integer shippingId) {
        Shipping shipping = shippingMapper.selectByUserId(id, shippingId);
        if (shipping == null) {
            return ServerResponse.createByErrorMessage(ShippingStatus.ILLEGAL_PARAM.getDesc());
        }
        return ServerResponse.createBySuccess(shipping);
    }

    @Override
    public ServerResponse list(Integer id, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Shipping> shippingList = shippingMapper.selectList(id);
        PageInfo pageInfo = new PageInfo(shippingList);
        return ServerResponse.createBySuccess(pageInfo);
    }


}
