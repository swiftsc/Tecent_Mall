package com.sc.service.impl;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.sc.common.CartStatus;
import com.sc.common.Const;
import com.sc.common.ServerResponse;
import com.sc.dao.CartMapper;
import com.sc.dao.ProductMapper;
import com.sc.pojo.Cart;
import com.sc.pojo.Category;
import com.sc.pojo.Product;
import com.sc.service.ICartService;
import com.sc.service.ICategoryService;
import com.sc.util.BigDecimalUtil;
import com.sc.util.PropertiesUtil;
import com.sc.vo.CartProductVo;
import com.sc.vo.CartVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by sc on 2017/10/21.
 */
@Service
@Slf4j
public class CartServiceImpl implements ICartService {

    @Autowired
    private CartMapper cartMapper;
    @Autowired
    private ProductMapper productMapper;

    @Override
    public ServerResponse add(Integer userId, Integer count, Integer productId) {
        if (productId == null || count == null) {
            return ServerResponse.createByErrorMessage(CartStatus.PARAM_ERROR.getDesc());
        }
        Cart cart = cartMapper.selectCartByUserIdProductId(userId, productId);
        if (cart == null) {
            Cart cartitem = new Cart();
            cartitem.setQuantity(count);
            cartitem.setChecked(CartStatus.Cart.checked);
            cartitem.setProductId(productId);
            cartitem.setUserId(userId);
            cartMapper.insert(cartitem);
        } else {
            cart.setQuantity(cart.getQuantity() + count);
            cartMapper.updateByPrimaryKey(cart);
        }
        return list(userId);

    }

    @Override
    public ServerResponse update(Integer userId, Integer count, Integer productId) {
        if (productId == null || count == null) {
            return ServerResponse.createByErrorMessage(CartStatus.PARAM_ERROR.getDesc());
        }
        Cart cart = cartMapper.selectCartByUserIdProductId(userId, productId);
        if (cart != null) {
            cart.setQuantity(count);
        }
        cartMapper.updateByPrimaryKeySelective(cart);
        return list(userId);
    }

    @Override
    public ServerResponse delete(Integer userId, String productIds) {
        List<String> productList = Splitter.on(",").splitToList(productIds);
        if (CollectionUtils.isEmpty(productList)) {
            return ServerResponse.createByErrorMessage(CartStatus.PARAM_ERROR.getDesc());
        }
        cartMapper.deleteByUserIdAndProductIds(userId, productList);
        return list(userId);
    }

    @Override
    public ServerResponse list(Integer id) {
        CartVo cartVo = getCartVoLimit(id);
        return ServerResponse.createBySuccess(cartVo);
    }

    @Override
    public ServerResponse selectorUnselect(Integer userId, Integer checked, Integer productId) {
        cartMapper.checkedOrUnchecked(userId, checked, productId);
        return list(userId);
    }

    @Override
    public ServerResponse getCartProductCount(Integer id) {
        if (id == null) {
            return ServerResponse.createBySuccess(0);
        }
        return ServerResponse.createBySuccess(cartMapper.selectCartProductCount(id));
    }


    private CartVo getCartVoLimit(Integer userId) {
        CartVo cartVo = new CartVo();
        List<Cart> cartList = cartMapper.selectCartByUserId(userId);
        List<CartProductVo> cartProductVoList = Lists.newArrayList();
        BigDecimal cartTotalPrice = new BigDecimal("0");
        if (CollectionUtils.isNotEmpty(cartList)) {
            for (Cart cart : cartList) {
                CartProductVo cartProductVo = new CartProductVo();
                cartProductVo.setId(cart.getId());
                cartProductVo.setUserId(userId);
                cartProductVo.setProductId(cart.getProductId());
                Product product = productMapper.selectByPrimaryKey(cart.getProductId());
                if (product != null) {
                    cartProductVo.setProductMainImage(product.getMainImage());
                    cartProductVo.setProductName(product.getName());
                    cartProductVo.setProductSubtitle(product.getSubtitle());
                    cartProductVo.setProductStatus(product.getStatus());
                    cartProductVo.setProductPrice(product.getPrice());
                    cartProductVo.setProductStock(product.getStock());
                    int buyLimit = 0;
                    if (product.getStock() >= cart.getQuantity()) {
                        buyLimit = cart.getQuantity();
                        cartProductVo.setLimitQuantity(CartStatus.Cart.LIMIT_COUNT_SUCCESS);
                    } else {
                        buyLimit = product.getStock();
                        cartProductVo.setLimitQuantity(CartStatus.Cart.LIMIT_COUNT_FAIL);
                        Cart cartQ = new Cart();
                        cartQ.setId(cart.getId());
                        cartQ.setQuantity(buyLimit);
                        cartMapper.updateByPrimaryKeySelective(cartQ);
                    }
                    cartProductVo.setQuantity(buyLimit);
                    cartProductVo.setProductTotalPrice(BigDecimalUtil.mul(product.getPrice().doubleValue(), cartProductVo.getQuantity()));
                    cartProductVo.setProductChecked(cart.getChecked());
                }
                if (cart.getChecked() == CartStatus.Cart.checked) {
                    cartTotalPrice = BigDecimalUtil.add(cartTotalPrice.doubleValue(), cartProductVo.getProductTotalPrice().doubleValue());
                }
                cartProductVoList.add(cartProductVo);
            }
        }
        cartVo.setCartTotalPrice(cartTotalPrice);
        cartVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));
        cartVo.setCartProductVoList(cartProductVoList);
        cartVo.setAllChecked(getIsAllChecked(userId));
        return cartVo;
    }

    private boolean getIsAllChecked(Integer userId) {
        return userId != null && cartMapper.selectCartProductCheckedStatusByUserId(userId) == 0;
    }
}
