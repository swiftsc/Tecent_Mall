package com.sc.common;

import lombok.Getter;

/**
 * Created by sc on 2017/10/22.
 */
@Getter
public enum ShippingStatus {
    ILLEGAL_PARAM(801, "参数错误"),
    DEL_SUCCESS(901, "删除地址成功"),
    DEL_FAIL(803, "删除地址失败"),
    UPDATE_SUCCESS(902, "更新地址成功"),
    UPDATE_FAIL(804, "更新地址失败"),
    ADD_ADDRESS_FAIL(802, "添加地址失败");

    private int code;
    private String desc;

    ShippingStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
