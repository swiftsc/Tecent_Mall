package com.sc.common;

import lombok.Getter;

/**
 * Created by sc on 2017/10/21.
 */
@Getter
public enum CartStatus {
    PARAM_ERROR(701, "参数异常"),
    CART_EMPTY(702,"购物车为空");


    private int code;
    private String desc;

    CartStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public interface Cart {
        int checked = 1;
        int unchecked = 0;
        String LIMIT_COUNT_SUCCESS = "限制成功";
        String LIMIT_COUNT_FAIL = "限制失败";
    }

}
