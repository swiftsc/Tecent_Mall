package com.sc.common;

import lombok.Getter;


public enum ResponseCode {
    SUCCESS(0, "SUCCESS"),
    ILLEGAL_ARGU(2, "参数错误"),
    ERROR(1, "ERROR");


    private final int code;
    private final String desc;

    ResponseCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
