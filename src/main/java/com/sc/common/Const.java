package com.sc.common;

import com.sc.pojo.User;

import javax.servlet.http.HttpSession;

/**
 * Created by sc on 2017/10/13.
 */
public class Const {
    public static final String CURRENT_USER = "currentUser";
    public static final String EMAIL = "email";
    public static final String USERNAME = "username";


    public interface RedisTimeOut{
        int REDIS_TIME_OUT = 60*30;
    }

    public interface REDIS_LOCK {
        String CLOSE_ORDER="CLOSE_ORDER";
    }
}
