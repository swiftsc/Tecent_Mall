package com.sc.common;

import com.sc.util.PropertiesUtil;
import redis.clients.jedis.*;
import redis.clients.util.Hashing;
import redis.clients.util.Sharded;

import java.util.Arrays;
import java.util.List;

/**
 * @author sc
 * Created on  2017/12/30
 */
public class RedisShardedPool {


    private static ShardedJedisPool pool;
    private static Integer maxTotal = Integer.parseInt(PropertiesUtil.getProperty("redis.max.total","20"));
    private static Integer maxIdle = Integer.parseInt(PropertiesUtil.getProperty("redis.max.idle","20"));
    private static Integer minIdle = Integer.parseInt(PropertiesUtil.getProperty("redis.min.idle","20"));

    private static Boolean testOnBorrow = Boolean.parseBoolean(PropertiesUtil.getProperty("redis.test.borrow","true"));
    private static Boolean testOnReturn = Boolean.parseBoolean(PropertiesUtil.getProperty("redis.test.return","true"));

    private static String redisIp = PropertiesUtil.getProperty("redis.ip");
    private static Integer redisPort = Integer.parseInt(PropertiesUtil.getProperty("redis.port"));

    private static String redis2Ip = PropertiesUtil.getProperty("redis2.ip");
    private static Integer redis2Port = Integer.parseInt(PropertiesUtil.getProperty("redis2.port"));


    private static void initPool(){
        JedisPoolConfig conf = new JedisPoolConfig();
        conf.setMaxTotal(maxTotal);
        conf.setMaxIdle(maxIdle);
        conf.setMinIdle(minIdle);
        conf.setTestOnBorrow(testOnBorrow);
        conf.setTestOnReturn(testOnReturn);
        conf.setBlockWhenExhausted(true);
        JedisShardInfo info1 = new JedisShardInfo(redisIp,redisPort);
        JedisShardInfo info2 = new JedisShardInfo(redis2Ip,redis2Port);
        List<JedisShardInfo> list = Arrays.asList(info1,info2);
        //MURMUR Hash就是一致性算法
        pool = new ShardedJedisPool(conf,list, Hashing.MURMUR_HASH, Sharded.DEFAULT_KEY_TAG_PATTERN);
    }

    static {
        initPool();
    }

    public static ShardedJedis   getJedis(){
        return pool.getResource();
    }
    public static void returnBrokenResource(ShardedJedis jedis){
        pool.returnBrokenResource(jedis);
    }
    public static void returnResource(ShardedJedis jedis){
        pool.returnResource(jedis);
    }


    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            ShardedJedis jedis = pool.getResource();
            jedis.set(i+"",i+"");
            returnResource(jedis);
        }
        pool.destroy();
        System.out.println("end");
    }
}
