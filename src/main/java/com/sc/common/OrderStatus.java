package com.sc.common;

import lombok.Getter;

import javax.annotation.Generated;

/**
 * Created by sc on 2017/10/22.
 */
@Getter
public enum OrderStatus {
    SHIPPED_SUCCESS(1006, "发货成功"),
    OPERATE_FAILED(1005, "操作失败,请重试"),
    CANT_CANCEL(1004, "已付款，无法取消"),
    ERROR_CREATE_ORDER(1003, "生成订单信息错误"),
    NO_ORDER(1001, "没有此订单信息"),
    CANCEL(0, "已取消"),
    NO_PAY(10, "未支付"),
    PAID(20, "已付款"),
    SHIPPED(40, "已发货"),
    ORDER_SUCCESS(50, "订单完成"),
    ORDER_CLOSE(60, "订单关闭"),
    ALIPAY_REPEAT_CALLBACK(1002, "支付宝重复调用");


    private int code;
    private String desc;

    OrderStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public static OrderStatus codeof(int code) {
        for (OrderStatus orderStatus : values()) {
            if (orderStatus.getCode() == code) {
                return orderStatus;
            }
        }
        throw new RuntimeException("没有找到对应枚举");
    }


    public interface AlipayCallBack {
        String WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
        String TRADE_SUCCESS = "TRADE_SUCCESS";
        String RESPONSE_SUCCESS = "success";
        String RESPONSE_FAILED = "fail";
    }

    @Getter
    public enum PayPlatForm {
        ALIPAY(1, "支付宝");

        private int code;
        private String desc;

        PayPlatForm(int code, String desc) {
            this.code = code;
            this.desc = desc;
        }
    }

    @Getter
    public enum PaymentTypeEnum {
        ONLINE_PAY(1, "在线支付");

        private int code;
        private String desc;

        PaymentTypeEnum(int code, String desc) {
            this.code = code;
            this.desc = desc;
        }

        public static PaymentTypeEnum codeof(int code) {
            for (PaymentTypeEnum paymentTypeEnum : values()) {
                if (paymentTypeEnum.getCode() == code) {
                    return paymentTypeEnum;
                }
            }
            throw new RuntimeException("没有找到对应枚举");
        }

    }


}
