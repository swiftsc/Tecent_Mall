package com.sc.common;

import lombok.Getter;

/**
 * Created by sc on 2017/10/13.
 */

@Getter
public enum UserStatus {
    NO_USER(100, "没有此用户"),
    REGISTER_SUCCESS(201, "注册成功"),
    LOGIN_SUCCESS(202, "登录成功"),
    PASSWORD_RESET_SUCCESS(203, "修改密码成功"),
    INFORMATION_MODIFY_SUCCESS(204, "个人信息修改成功"),
    PWD_ERROR(101, "密码错误"),
    EMAIL_EXIST(102, "邮箱已存在"),
    REGISTER_FAILED(104, "注册失败"),
    NEED_LOGIN(105, "需要登录"),
    QUESTION_EMPTY(106, "找回密码问题为空"),
    ANSWER_ERROR(107, "问题答案错误"),
    TOKEN_EMPTY(108, "token为空"),
    TOKEN_INVALID(109, "token无效或者过期"),
    PASSWORD_RESET_FAILED(110, "修改密码失败，请重试"),
    OLDPWD_ERROR(111, "旧密码错误"),
    INFORMATION_MODIFY_FAILED(112, "个人信息修改失败"),
    NO_AUTHORITY(113, "没有权限"),
    PWD_NO_CHANGE(114, "密码没有改变"),
    USERNAME_EXIST(103, "用户名已存在");

    public static final String EMAIL = "email";
    public static final String USERNAME = "username";


    private int code;
    private String desc;

    UserStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    public interface Role {
        int ROLE_CUSTOMER = 0; //用户
        int ROLE_ADMIN = 1; //管理员
        int ROLE_SUPPERADMIN = 100;//超管
    }
}
