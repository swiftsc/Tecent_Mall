package com.sc.common;

import com.google.common.collect.Sets;
import lombok.Getter;

import java.util.Set;

/**
 * Created by sc on 2017/10/15.
 */

@Getter
public enum ProductStatus {
    NO_ENOUGH_STOCK(605, "没有足够库存"),
    UPDATE_SUCCESS(501, "商品信息更新成功"),
    UPDATE_FAILED(601, "商品信息更新失败"),
    SAVE_SUCCESS(502, "新增商品成功"),
    SAVE_FAILED(602, "新增商品失败"),
    NO_PRODUCT(604, "没有此产品"),
    ILLEGAL_PARAM(603, "参数非法"),
    ON_SALE(1, "在售状态");


    private int code;
    private String desc;

    ProductStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public interface ProductListOrderBy {
        Set<String> PRICE_ASC_DESC = Sets.newHashSet("price_asc", "price_desc");

    }


}
