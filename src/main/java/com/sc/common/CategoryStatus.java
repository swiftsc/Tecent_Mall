package com.sc.common;

import lombok.Getter;

/**
 * Created by sc on 2017/10/15.
 */
@Getter
public enum CategoryStatus {
    ILLEGAL_PARAM(301, "参数错误"),
    ADD_FAILED(302, "添加品类错误"),
    NO_CATEGORY(303, "没有此品类"),
    MODIFY_FAILED(304, "修改品类信息失败"),
    MODIFY_SUCCESS(402, "修改品类信息成功"),
    PARENT_ID_ERROR(305, "没有此父节点信息"),
    ADD_SUCCESS(401, "添加品类成功");

    private int code;
    private String desc;

    CategoryStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

}
