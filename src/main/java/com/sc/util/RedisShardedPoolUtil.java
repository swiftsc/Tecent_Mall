package com.sc.util;

import com.sc.common.RedisPool;
import com.sc.common.RedisShardedPool;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.ShardedJedis;

/**
 * @author sc
 * Created on  2017/12/28
 */
@Slf4j
public class RedisShardedPoolUtil {

    public static String set(String key,String value){
        ShardedJedis jedis = null;
        String result= null;
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.set(key, value);
        } catch (Exception e) {
            log.info("set key:{} value:{} error",key,value);
            RedisShardedPool.returnBrokenResource(jedis);
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }


    public static String get(String key){
        ShardedJedis jedis = null;
        String reslut= null;
        try {
            jedis = RedisShardedPool.getJedis();
            reslut = jedis.get(key);
        } catch (Exception e) {
            log.info("get key:{} error",key);
            RedisShardedPool.returnBrokenResource(jedis);
            return reslut;
        }
        RedisShardedPool.returnResource(jedis);
        return reslut;
    }

    /**
     * 时间单位是秒
     */
    public static String setEx(String key,String value,int exTime){
        ShardedJedis jedis = null;
        String result= null;
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.setex(key,exTime,value);
        } catch (Exception e) {
            log.info("setex key:{} value:{} error",key,value);
            RedisShardedPool.returnBrokenResource(jedis);
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }


    public static Long expire(String key,int exTime){
        ShardedJedis jedis = null;
        Long result= null;
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.expire(key,exTime);
        } catch (Exception e) {
            log.info("expire key:{}  error",key);
            RedisShardedPool.returnBrokenResource(jedis);
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }

    public static Long del(String key){
        ShardedJedis jedis = null;
        Long result= null;
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.del(key);
        } catch (Exception e) {
            log.info("del key:{}  error",key);
            RedisShardedPool.returnBrokenResource(jedis);
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }


    public static Long setnx(String key,String value){
        ShardedJedis jedis = null;
        Long result= null;
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.setnx(key, value);
        } catch (Exception e) {
            log.info("setnx key:{} value:{} error",key,value);
            RedisShardedPool.returnBrokenResource(jedis);
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }

    public static String getSet(String key,String value){
        ShardedJedis jedis = null;
        String result= null;
        try {
            jedis = RedisShardedPool.getJedis();
            result = jedis.getSet(key, value);
        } catch (Exception e) {
            log.info("getSet key:{} value:{} error",key,value);
            RedisShardedPool.returnBrokenResource(jedis);
            return result;
        }
        RedisShardedPool.returnResource(jedis);
        return result;
    }

}
