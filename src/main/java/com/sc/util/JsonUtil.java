package com.sc.util;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * @author sc
 * Created on  2017/12/28
 */
@Slf4j
public class JsonUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();
    static{
        //对象的字段全部加入
         objectMapper.setSerializationInclusion(JsonSerialize.Inclusion.ALWAYS);
        //取消timestamp的格式转换
         objectMapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS,false);
         objectMapper.configure(SerializationConfig.Feature.FAIL_ON_EMPTY_BEANS,false);
         objectMapper.setDateFormat(new SimpleDateFormat(DateTimeUtil.STANDARD_FORMAT));

         //反序列化
        //忽略json字符串中存在，pojo中不存在的情况。
        objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,false);
    }


    public static <T> String obj2Str(T obj)  {
        if(obj==null){
            return null;
        }
        try {
            return obj instanceof String ? (String) obj :objectMapper.writeValueAsString(obj);
        } catch (IOException e) {
            log.warn("json Parse Error:{}",e);
            e.printStackTrace();
            return null;
        }
    }
    public static <T> String obj2StrPretty(T obj)  {
        if(obj==null){
            return null;
        }
        try {
            return obj instanceof String ? (String) obj :objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (IOException e) {
            log.warn("json Parse Error:{}",e);
            e.printStackTrace();
            return null;
        }
    }


    public static <T> T str2Obj(String json,Class<T> clazz){
        if(StringUtils.isEmpty(json)||clazz==null){
            return null;
        }
        try {
            return clazz.equals(String.class)? (T) json :objectMapper.readValue(json,clazz);
        } catch (IOException e) {
            log.warn("Parse String 2 obj error:{}",e);
            e.printStackTrace();
            return null;
        }
    }


    public static <T> T str2Obj(String str, TypeReference<T> reference){
        if(StringUtils.isEmpty(str)||reference==null){
            return null;
        }
        try {
            return reference.getType().equals(String.class)? (T) str :objectMapper.readValue(str,reference);
        } catch (IOException e) {
            log.warn("Parse String 2 obj error:{}",e);
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T str2Obj(String str,Class<?> collectionClass,Class<?>... elementClasses){
        JavaType javaType = objectMapper.getTypeFactory().constructParametricType(collectionClass,elementClasses);

        try {
            return objectMapper.readValue(str,javaType);
        } catch (IOException e) {
            log.warn("Parse String 2 obj error:{}",e);
            e.printStackTrace();
            return null;
        }
    }


}
