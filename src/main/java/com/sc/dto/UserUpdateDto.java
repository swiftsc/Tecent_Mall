package com.sc.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.Pattern;

/**
 * Created by sc on 2017/10/14.
 * 可以修改的用户信息
 */
@Data
public class UserUpdateDto {

    private Integer id;
    @Email(message = "邮箱格式错误")
    private String email;

    @Pattern(regexp = "^1(3[0-9]|4[57]|5[0-35-9]|7[01678]|8[0-9])\\d{8}$", message = "手机号格式错误")
    private String phone;

    private String question;

    private String answer;
}
