package com.sc.controller.backend;

import com.sc.common.Const;
import com.sc.common.ServerResponse;
import com.sc.common.UserStatus;
import com.sc.pojo.User;
import com.sc.service.IUserService;
import com.sc.util.CookieUtil;
import com.sc.util.JsonUtil;
import com.sc.util.RedisShardedPoolUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by sc on 2017/10/14.
 */
@Controller
@RequestMapping("/manage/user")
public class UserManageController {

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> login(HttpServletResponse httpServletResponse,String username, String password, HttpSession session) {
        ServerResponse<User> response = userService.login(username, password);
        if (response.isSuccess()) {
            User user = response.getData();
            if (user.getRole() == UserStatus.Role.ROLE_ADMIN) {
                CookieUtil.writeLoginToken(httpServletResponse,session.getId());
                RedisShardedPoolUtil.setEx(session.getId(), JsonUtil.obj2Str(response.getData()), Const.RedisTimeOut.REDIS_TIME_OUT);
                return response;
            } else {
                return ServerResponse.createByErrorMessage(UserStatus.NO_AUTHORITY.getDesc());
            }
        }
        return response;
    }

}
