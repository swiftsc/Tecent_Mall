package com.sc.controller.backend;

import com.google.common.collect.Maps;
import com.sc.common.ProductStatus;
import com.sc.common.ServerResponse;
import com.sc.common.UserStatus;
import com.sc.pojo.Product;
import com.sc.pojo.User;
import com.sc.service.IFileService;
import com.sc.service.IProductService;
import com.sc.service.IUserService;
import com.sc.util.CookieUtil;
import com.sc.util.JsonUtil;
import com.sc.util.PropertiesUtil;
import com.sc.util.RedisShardedPoolUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Created by sc on 2017/10/15.
 */
@Controller
@RequestMapping("/manage/product")
public class ProductManageController {

    @Autowired
    private IUserService userService;
    @Autowired
    private IProductService productService;
    @Autowired
    private IFileService fileService;

    @RequestMapping("/save")
    @ResponseBody
    public ServerResponse productSave(@Valid Product product, BindingResult result) {
        String errors = "错误";
        StringBuilder stringBuilder = new StringBuilder();
        if (result.hasErrors()) {
            List<ObjectError> errorList = result.getAllErrors();
            for (ObjectError error : errorList) {
               stringBuilder.append(error+"||");
            }
            errors = errors +stringBuilder;
            return ServerResponse.createByErrorMessage(errors);
        }
        return productService.saveOrUpdate(product);
    }

    @RequestMapping("/set_sale_status")
    @ResponseBody
    public ServerResponse setSaleStatus(@RequestParam("productId") Integer productId, @RequestParam("status") Integer status) {
        if (productId != null && status != null) {
            return productService.setSaleStatus(productId, status);
        }
        return ServerResponse.createByErrorMessage(ProductStatus.ILLEGAL_PARAM.getDesc());
    }

    @RequestMapping("/detail")
    @ResponseBody
    public ServerResponse getDetail( @RequestParam("productId") Integer productId) {

        if (productId != null) {
            return productService.manageProductDetail(productId);
        }
        return ServerResponse.createByErrorMessage(ProductStatus.ILLEGAL_PARAM.getDesc());
    }

    @RequestMapping("/list")
    @ResponseBody
    public ServerResponse getList(
                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        return productService.getList(pageNum, pageSize);
    }

    @RequestMapping("/search")
    @ResponseBody
    public ServerResponse getList(
                                  String productName,
                                  Integer productId,
                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        return productService.searchProduct(productName, productId, pageNum, pageSize);

    }


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse upload(MultipartFile file, HttpServletRequest request) {
        String path = request.getSession().getServletContext().getRealPath("upload");
        System.out.println("upload");
        String filename = fileService.upload(file, path);
        String url = PropertiesUtil.getProperty("ftp.server.http.prefix") + filename;
        Map filemap = Maps.newHashMap();
        filemap.put("uri", filename);
        filemap.put("url", url);
        System.out.println(url);
        return ServerResponse.createBySuccess(filemap);
    }

    @RequestMapping(value = "/richtext_img_upload", method = RequestMethod.POST)
    @ResponseBody
    public Map richtext(MultipartFile file, HttpServletRequest request,
                        HttpServletResponse response) {
        Map result = Maps.newHashMap();
        String path = request.getSession().getServletContext().getRealPath("upload");
        System.out.println("upload");
        String filename = fileService.upload(file, path);
        if (StringUtils.isBlank(filename)) {
            result.put("msg", "网络故障请重试");
            result.put("success", false);
            return result;
        }
        String url = PropertiesUtil.getProperty("ftp.server.http.prefix") + filename;
        //富文本按照simditor的要求进行返回

        result.put("success", true);
        result.put("msg", "上传成功");
        result.put("url", url);
        response.addHeader("Access-Control-Allow-Headers", "X-File-Name");
        return result;
    }
}
