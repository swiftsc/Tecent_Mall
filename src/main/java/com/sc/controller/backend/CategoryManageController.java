package com.sc.controller.backend;

import com.sc.common.ServerResponse;
import com.sc.common.UserStatus;
import com.sc.pojo.User;
import com.sc.service.ICategoryService;
import com.sc.service.IUserService;
import com.sc.util.CookieUtil;
import com.sc.util.JsonUtil;
import com.sc.util.RedisShardedPoolUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by sc on 2017/10/15.
 */
@Controller
@RequestMapping("/manage/category")
public class CategoryManageController {

    @Autowired
    private ICategoryService categoryService;
    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/add_category", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse addCategory(
                                      @RequestParam(value = "categoryName", required = true) String categoryName,
                                      @RequestParam(value = "parentId", defaultValue = "0") Integer parentId) {

        return categoryService.add(categoryName, parentId);
    }

    @RequestMapping("/set_category_name")
    @ResponseBody
    public ServerResponse setCategoryName(
                                          @RequestParam(value = "categoryName", required = true) String categoryName,
                                          @RequestParam(value = "categoryId", required = true, defaultValue = "0") Integer categoryId) {
        return categoryService.updateCategory(categoryName, categoryId);
    }

    @RequestMapping("/get_category")
    @ResponseBody
    public ServerResponse findChildParallelCategory(
                                                    @RequestParam(value = "categoryId", required = true, defaultValue = "0") Integer categoryId) {

        return categoryService.selectParallelCategory(categoryId);

    }

    @RequestMapping("/get_deep_category")
    @ResponseBody
    public ServerResponse findAllChildCategory( @RequestParam(value = "categoryId", defaultValue = "0") Integer parentId) {

        return categoryService.selectDeepCategory(parentId);
    }

}
