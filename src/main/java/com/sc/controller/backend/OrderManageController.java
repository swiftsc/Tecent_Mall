package com.sc.controller.backend;


import com.sc.common.ServerResponse;
import com.sc.common.UserStatus;
import com.sc.pojo.User;
import com.sc.service.IOrderService;
import com.sc.service.IUserService;
import com.sc.util.CookieUtil;
import com.sc.util.JsonUtil;
import com.sc.util.RedisShardedPoolUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;


/**
 * Created by sc on 2017/10/28.
 */
@Controller
@RequestMapping("/manage/order")
public class OrderManageController {
    @Autowired
    private IUserService userService;
    @Autowired
    private IOrderService orderService;

    @RequestMapping("/list")
    @ResponseBody
    public ServerResponse orderList(
                                    @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                    @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize
    ) {
        return orderService.manageOrderList(pageNum, pageSize);
    }


    @RequestMapping("/detail")
    @ResponseBody
    public ServerResponse manageDetail( Long orderNo) {

        return orderService.manageOrderDetail(orderNo);
    }

    @RequestMapping("/search")
    @ResponseBody
    public ServerResponse orderSearch( Long orderNo,
                                      @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                      @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {

        return orderService.search(orderNo, pageNum, pageSize);
    }

    @RequestMapping("/send_goods")
    @ResponseBody
    public ServerResponse sendGoods(HttpServletRequest request, Long orderNo) {

        return orderService.sendGoods(orderNo);
    }
}
