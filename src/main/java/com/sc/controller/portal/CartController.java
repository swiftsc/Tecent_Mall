package com.sc.controller.portal;

import com.sc.common.CartStatus;
import com.sc.common.ServerResponse;
import com.sc.common.UserStatus;

import com.sc.pojo.User;
import com.sc.service.ICartService;
import com.sc.util.CookieUtil;
import com.sc.util.JsonUtil;
import com.sc.util.RedisShardedPoolUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by sc on 2017/10/21.
 */
@Controller
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private ICartService cartService;

    @RequestMapping("/add")
    @ResponseBody
    public ServerResponse createCart(HttpServletRequest request, Integer count, Integer productId) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return cartService.add(user.getId(), count, productId);
    }

    @RequestMapping("/update")
    @ResponseBody
    public ServerResponse update(HttpServletRequest request, Integer count, Integer productId) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return cartService.update(user.getId(), count, productId);
    }

    @RequestMapping("/delete_product")
    @ResponseBody
    public ServerResponse delete(HttpServletRequest request, String productIds) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return cartService.delete(user.getId(), productIds);
    }

    @RequestMapping("/list")
    @ResponseBody
    public ServerResponse list(HttpServletRequest request) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return cartService.list(user.getId());
    }

    //全选全反选 单独选 单独反选 查询当前购物车产品数量
    @RequestMapping("/select_all")
    @ResponseBody
    public ServerResponse selectAll(HttpServletRequest request) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return cartService.selectorUnselect(user.getId(), CartStatus.Cart.checked, null);
    }

    @RequestMapping("/un_select_all")
    @ResponseBody
    public ServerResponse unSelectAll(HttpServletRequest request) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return cartService.selectorUnselect(user.getId(), CartStatus.Cart.unchecked, null);
    }

    @RequestMapping("/un_select")
    @ResponseBody
    public ServerResponse unSelect(HttpServletRequest request, Integer productId) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return cartService.selectorUnselect(user.getId(), CartStatus.Cart.unchecked, productId);
    }

    @RequestMapping("/select")
    @ResponseBody
    public ServerResponse Select(HttpServletRequest request, Integer productId) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return cartService.selectorUnselect(user.getId(), CartStatus.Cart.checked, productId);
    }

    @RequestMapping("/get_cart_product_count")
    @ResponseBody
    public ServerResponse getCartProductCount(HttpServletRequest request) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createBySuccess(0);
        }
        return cartService.getCartProductCount(user.getId());
    }

}
