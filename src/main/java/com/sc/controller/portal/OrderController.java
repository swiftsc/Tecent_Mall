package com.sc.controller.portal;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.demo.trade.config.Configs;
import com.google.common.collect.Maps;
import com.sc.common.OrderStatus;
import com.sc.common.ServerResponse;
import com.sc.common.UserStatus;
import com.sc.pojo.User;
import com.sc.service.IOrderService;
import com.sc.util.CookieUtil;
import com.sc.util.JsonUtil;
import com.sc.util.RedisShardedPoolUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by sc on 2017/10/22.
 */
@Controller
@RequestMapping("/order")
public class OrderController {
    private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private IOrderService orderService;

    @RequestMapping("/pay")
    @ResponseBody
    public ServerResponse pay( Long orderNo, HttpServletRequest request) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        String path = request.getSession().getServletContext().getRealPath("upload");
        return orderService.pay(user.getId(), orderNo, path);
    }

    @RequestMapping("/alipay_callback")
    @ResponseBody
    public Object callBack(HttpServletRequest request) {
        Map<String, String> params = Maps.newHashMap();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valuestr = "";
            for (int i = 0; i < values.length; i++) {
                valuestr = (i == values.length - 1) ? valuestr + values[i] : valuestr + values[i] + ",";
            }
            params.put(name, valuestr);
        }
        logger.info("支付宝回调,sign:{},trade_status{},参数{}", params.get("sign"), params.get("trade_status"), params.toString());
        //验证回调正确性 避免重复通知
        params.remove("sign_type");
        try {
            boolean alipayRSACheckedV2 = AlipaySignature.rsaCheckV2(params, Configs.getAlipayPublicKey(), "utf-8", Configs.getSignType());
            if (!alipayRSACheckedV2) {
                return ServerResponse.createByErrorMessage("非法请求，验证不通过");
            }
        } catch (AlipayApiException e) {
            logger.error("alipay异常" + e);
        }
        // TODO: 2017/10/22 验证数据
        ServerResponse response = orderService.aliCallBack(params);
        if (response.isSuccess()) {
            return OrderStatus.AlipayCallBack.RESPONSE_SUCCESS;
        }
        return OrderStatus.AlipayCallBack.RESPONSE_FAILED;
    }

    @RequestMapping("/query_order_pay_status")
    @ResponseBody
    public ServerResponse queryOrderPayStatus(HttpServletRequest request, Long orderNo) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        ServerResponse response = orderService.query(user.getId(), orderNo);
        if (response.isSuccess()) {
            return ServerResponse.createBySuccess(true);
        }
        return ServerResponse.createBySuccess(false);
    }

    @RequestMapping("/cancel")
    @ResponseBody
    public ServerResponse cancel(HttpServletRequest request, Long orderNo) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return orderService.cancelOrder(user.getId(), orderNo);
    }


    @RequestMapping("/create")
    @ResponseBody
    public ServerResponse create(HttpServletRequest request, Integer shippingId) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return orderService.createOrder(user.getId(), shippingId);
    }

    @RequestMapping("/get_order_cart_product")
    @ResponseBody
    public ServerResponse getOrderCartProduct(HttpServletRequest request) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return orderService.getOrderCartProduct(user.getId());
    }

    @RequestMapping("/detail")
    @ResponseBody
    public ServerResponse detail(HttpServletRequest request, Long orderNo) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return orderService.getDetail(user.getId(), orderNo);
    }


    @RequestMapping("/list")
    @ResponseBody
    public ServerResponse getList(HttpServletRequest request,
                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                  @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return orderService.getList(user.getId(), pageNum, pageSize);
    }
}
