package com.sc.controller.portal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by sc on 2017/10/19.
 */
@Controller
public class IndexController {
    @RequestMapping("/index")
    public ModelAndView index() {
        return new ModelAndView("index");
    }
}
