package com.sc.controller.portal;

import com.sc.common.ServerResponse;
import com.sc.service.IProductService;
import com.sc.vo.ProductDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by sc on 2017/10/21.
 */
@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private IProductService productService;

    @RequestMapping("/detail")
    @ResponseBody
    public ServerResponse<ProductDetailVo> getdetial(Integer productId) {
        return productService.getDetail(productId);
    }

    @RequestMapping("/list")
    @ResponseBody
    public ServerResponse List(@RequestParam(value = "keyword", required = false) String keyword,
                               @RequestParam(value = "categoryId", required = false) Integer categoryId,
                               @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                               @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize,
                               @RequestParam(value = "orderBy", defaultValue = "") String orderBy) {
        return productService.getProductByKeywordAndCategory(pageNum, pageSize, keyword, categoryId, orderBy);
    }
}
