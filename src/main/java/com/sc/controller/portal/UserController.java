package com.sc.controller.portal;

import com.sc.common.Const;
import com.sc.common.ServerResponse;
import com.sc.common.UserStatus;
import com.sc.dto.UserUpdateDto;
import com.sc.pojo.User;
import com.sc.service.IUserService;


import com.sc.util.CookieUtil;
import com.sc.util.JsonUtil;
import com.sc.util.RedisShardedPoolUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> login(String username, String password, HttpSession session, HttpServletResponse httpServletResponse) {
        ServerResponse<User> response = userService.login(username, password);
        if (response.isSuccess()) {
            CookieUtil.writeLoginToken(httpServletResponse,session.getId());
            RedisShardedPoolUtil.setEx(session.getId(), JsonUtil.obj2Str(response.getData()), Const.RedisTimeOut.REDIS_TIME_OUT);
        }
        return response;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<String> logout(HttpServletRequest request,HttpServletResponse response) {
        CookieUtil.delLoginToken(request, response);
        RedisShardedPoolUtil.del(CookieUtil.getLoginToken(request));
        return ServerResponse.createBySuccess();
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<String> register(@Valid User user, BindingResult result) {
        String errors = "错误";
        if (result.hasErrors()) {
            List<ObjectError> errorList = result.getAllErrors();
            for (ObjectError error : errorList) {
                errors = errors + "||" + error;
            }
            return ServerResponse.createByErrorMessage(errors);
        }
        return userService.register(user);

    }

    @RequestMapping(value = "/get_user_info", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<User> getUserInfo(HttpServletRequest request) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user != null) {
            return ServerResponse.createBySuccess(user);
        }
        return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
    }


    @RequestMapping(value = "/forget_get_question", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<String> forgetGetQues(String username) {
        return userService.selectQuestion(username);
    }

    @RequestMapping(value = "/forget_get_answer", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<String> forgetCheckAnswer(String username, String question, String answer) {
        return userService.checkAnswer(username, question, answer);
    }

    @RequestMapping(value = "/forget_reset_password", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<String> forgetResetPassword(String username, String passwordnew, String token) {
        return userService.forgetResetPassword(username, passwordnew, token);
    }

    @RequestMapping(value = "/reset_password", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<String> resetPassword(HttpServletRequest request, String passwordold, String passwordnew) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        if (StringUtils.equals(passwordnew, passwordold)) {
            return ServerResponse.createByErrorMessage(UserStatus.PWD_NO_CHANGE.getDesc());
        }
        return userService.resetPassword(user, passwordold, passwordnew);
    }

    @RequestMapping(value = "/update_information", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse<User> update_information(HttpServletRequest request,HttpServletResponse servletRes, @Valid UserUpdateDto user) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User currentUser = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (currentUser == null) {
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        user.setId(currentUser.getId());
        ServerResponse<User> response = userService.updateInformation(user);
        if (response.isSuccess()) {
            CookieUtil.writeLoginToken(servletRes,loginToken);
        }
        return response;
    }

    @RequestMapping(value = "/get_information", method = RequestMethod.GET)
    @ResponseBody
    public ServerResponse<User> get_information(HttpServletRequest request) {
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isEmpty(loginToken)){
            return ServerResponse.createByErrorMessage(UserStatus.NEED_LOGIN.getDesc());
        }
        User user = JsonUtil.str2Obj(RedisShardedPoolUtil.get(loginToken), User.class);
        if (user == null) {
            return ServerResponse.createByErrorCodeMessage(UserStatus.NEED_LOGIN.getCode(), UserStatus.NEED_LOGIN.getDesc());
        }
        return userService.getinformation(user.getId());

    }


}
