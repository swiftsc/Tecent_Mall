package com.sc.controller.filter;

import com.sc.common.Const;
import com.sc.pojo.User;
import com.sc.util.CookieUtil;
import com.sc.util.JsonUtil;
import com.sc.util.RedisShardedPoolUtil;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author sc
 * Created on  2017/12/29
 */
public class SessionExpireFilter implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        String loginToken = CookieUtil.getLoginToken(request);

        if(StringUtils.isNotBlank(loginToken)){
            String userJson = RedisShardedPoolUtil.get(loginToken);
            User user = JsonUtil.str2Obj(userJson,User.class);
            if(user!=null){
                RedisShardedPoolUtil.expire(loginToken, Const.RedisTimeOut.REDIS_TIME_OUT);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
