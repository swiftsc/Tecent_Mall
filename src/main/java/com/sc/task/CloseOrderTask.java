package com.sc.task;

import com.sc.common.Const;
import com.sc.common.RedisShardedPool;
import com.sc.common.RedissonManager;
import com.sc.service.IOrderService;
import com.sc.util.PropertiesUtil;
import com.sc.util.RedisShardedPoolUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.concurrent.TimeUnit;

/**
 * @author sc
 * Created on  2018/1/4
 */

@Component
@Slf4j
public class CloseOrderTask {
    @Autowired
    private IOrderService orderService;
    @Autowired
    private RedissonManager redissonManager;


    /**
     * 3 min
     */
    //@Scheduled(cron = "0 */1 * * * ?")
    public void closeOrderTask(){
        log.info("关闭订单任务启动");
        int hour = Integer.parseInt(PropertiesUtil.getProperty("order.closeTime","2"));
        orderService.closeOrder(hour);
        log.info("关闭订单任务完成");
    }

    //@Scheduled(cron = "0 */1 * * * ?")
    public void closeOrderTaskWithRedis(){
        log.info("close order start");
        long lockTimeOut = Long.parseLong(PropertiesUtil.getProperty("lock.timeout","5000"));
        Long setnx = RedisShardedPoolUtil.setnx(Const.REDIS_LOCK.CLOSE_ORDER, String.valueOf(System.currentTimeMillis() + lockTimeOut));
        if(setnx!=null&&setnx.intValue()==1){
            closeOrder(Const.REDIS_LOCK.CLOSE_ORDER);
        }else{
            log.info("cant get lock");
        }
        log.info("close order finish");
    }

    //@Scheduled(cron = "0 */1 * * * ?")
    public void closeOrderTaskV3(){
        log.info("close order start");
        long lockTimeOut = Long.parseLong(PropertiesUtil.getProperty("lock.timeout","5000"));
        Long setnx = RedisShardedPoolUtil.setnx(Const.REDIS_LOCK.CLOSE_ORDER, String.valueOf(System.currentTimeMillis() + lockTimeOut));
        if(setnx!=null&&setnx.intValue()==1){
            closeOrder(Const.REDIS_LOCK.CLOSE_ORDER);
        }else{
           //未获取到锁 判断时间戳 重置并获取
            String lockValue = RedisShardedPoolUtil.get(Const.REDIS_LOCK.CLOSE_ORDER);
            if(lockValue !=null && System.currentTimeMillis()>Long.parseLong(lockValue)){
                String old = RedisShardedPoolUtil.getSet(Const.REDIS_LOCK.CLOSE_ORDER, String.valueOf(System.currentTimeMillis() + lockTimeOut));
                if(old==null ||(old!=null&& StringUtils.equals(lockValue,old))){
                    //真正获取到锁
                    closeOrder(Const.REDIS_LOCK.CLOSE_ORDER);
                }else{
                    log.info("cant get lock");
                }
            }else{
                log.info("cant get lock");
            }
        }
        log.info("close order finish");
    }


    /**
     * 集成redisson
     */
    @Scheduled(cron = "0 */1 * * * ?")
    public void closeOrderTaskV4(){
        RLock lock = redissonManager.getRedisson().getLock(Const.REDIS_LOCK.CLOSE_ORDER);
        Boolean getLock = false;
        try {
            //第一个wait_time参数最好统一设置成0，任务完成即自动关闭，否则可能出现竞争锁的线程同时得到锁
            if(getLock = lock.tryLock(0,5, TimeUnit.SECONDS)){
                log.info("get lock");
                int hour = Integer.parseInt(PropertiesUtil.getProperty("order.closeTime","2"));
                orderService.closeOrder(hour);
            }else {
                log.info("cant get lock");
            }
        } catch (InterruptedException e) {
            log.error("try lock failed error:{}",e);
        }finally {
            if(!getLock){
                return;
            }else {
                lock.unlock();
            }
            log.info("finally unlock");
        }
    }





    private void closeOrder(String lockName){
        RedisShardedPoolUtil.expire(lockName,5  );
        int hour = Integer.parseInt(PropertiesUtil.getProperty("order.closeTime","2"));
        orderService.closeOrder(hour);
        RedisShardedPoolUtil.del(lockName);
    }


    /**
     * tomcat使用shutdown命令会执行这个方法
     */
    @PreDestroy
    public void delLock(){
        RedisShardedPoolUtil.del(Const.REDIS_LOCK.CLOSE_ORDER);
    }
}
