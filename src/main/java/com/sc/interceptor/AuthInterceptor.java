package com.sc.interceptor;

import com.google.common.collect.Maps;
import com.sc.common.ServerResponse;
import com.sc.common.UserStatus;
import com.sc.pojo.User;
import com.sc.util.CookieUtil;
import com.sc.util.JsonUtil;
import com.sc.util.RedisShardedPoolUtil;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

/**
 * @author sc
 * Created on  2018/1/3
 */
@Slf4j
public class AuthInterceptor implements HandlerInterceptor{


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HandlerMethod method = (HandlerMethod) handler;

        //解析HandlerMethod
        String methodName = method.getMethod().getName();
        String className = method.getBean().getClass().getSimpleName();

        //解析参数 取出key-value
        StringBuffer requestParamBuffer = new StringBuffer();
        Map paramMap = request.getParameterMap();
        Iterator it = paramMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String mapKey = (String) entry.getKey();
            String mapValue = StringUtils.EMPTY;
            //request 中的map  value返回是一个数组
            Object obj = entry.getValue();
            if (obj instanceof String[]) {
                String[] strs = (String[]) obj;
                mapValue = Arrays.toString(strs);
            }
            requestParamBuffer.append(mapKey).append("=").append(mapValue);

        }

        log.info("拦截请求,className:{},methodName:{},param:{}",className,methodName,requestParamBuffer);

        User user = null;
        String loginToken = CookieUtil.getLoginToken(request);
        if(StringUtils.isNotEmpty(loginToken)){
            String userJson = RedisShardedPoolUtil.get(loginToken);
            user = JsonUtil.str2Obj(userJson,User.class);
        }
        if(user==null|| (user.getRole().intValue()!= UserStatus.Role.ROLE_ADMIN)){
            //返回false 不会调用controller中的方法
            response.reset();
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter out = response.getWriter();


            //这里对富文本上传进行额外的配置
            if(user==null){
                if(StringUtils.equals(className,"ProductManageController")&&StringUtils.equals(methodName,"richtext")){
                    Map result = Maps.newHashMap();
                    result.put("success",false);
                    result.put("msg","请登录管理员");
                    out.print(JsonUtil.obj2Str(result));
                }else {
                    out.print(JsonUtil.obj2Str(ServerResponse.createByErrorMessage("管理员未登录")));
                }
            }else {
                if (StringUtils.equals(className, "ProductManageController") && StringUtils.equals(methodName, "richtext")) {
                    Map result = Maps.newHashMap();
                    result.put("success",false);
                    result.put("msg","无权限操作");
                    out.print(JsonUtil.obj2Str(result));
                } else {
                    out.print(JsonUtil.obj2Str(ServerResponse.createByErrorMessage("无权限登录")));
                }
            }
            out.flush();
            out.close();
            return false;
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
        //所有处理完才会调用

    }


}
