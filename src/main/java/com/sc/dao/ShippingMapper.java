package com.sc.dao;

import com.sc.pojo.Shipping;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShippingMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Shipping record);

    int insertSelective(Shipping record);

    Shipping selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Shipping record);

    int updateByPrimaryKey(Shipping record);

    int deleteByUserIdAndShippingId(@Param("userId") Integer id, @Param("shippingId") Integer shippingId);

    int updateByUserId(Shipping record);

    Shipping selectByUserId(@Param("userId") Integer id, @Param("shippingId") Integer shippingId);

    List<Shipping> selectList(@Param("userId") Integer id);
}