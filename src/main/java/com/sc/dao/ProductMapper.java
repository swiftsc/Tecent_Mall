package com.sc.dao;

import com.sc.pojo.Product;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);

    int updateStatusById(@Param("productId") Integer productId, @Param("status") Integer status);

    List<Product> selectList();

    List<Product> selectByNameAndProductId(@Param("productId") Integer productId, @Param("productName") String productName);

    List<Product> selectByNameAndCategory(@Param("productName") String keyword, @Param("categoryIdList") List<Integer> categoryIdList);

    Integer selectStockByProductId(@Param("productId") Integer productId);
}