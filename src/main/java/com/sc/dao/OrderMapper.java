package com.sc.dao;

import com.sc.common.OrderStatus;
import com.sc.pojo.Order;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);

    Order selectByUserIdAndOrderNo(@Param("orderNo") Long orderNo, @Param("userId") Integer userId);

    Order selectByOrderNo(Long orderNo);

    List<Order> selectAll(@Param("userID") Integer id);

    List<Order> selectAllAdmin();

    List<Order> selectorderstatusbycreatetime(@Param("status") int noPay, @Param("date") String closeDateTime);

    void closeOrderById(@Param("id") Integer id);
}